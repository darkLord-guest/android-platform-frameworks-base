NAME = libandroidfw
SOURCES = \
        ApkAssets.cpp \
        Asset.cpp \
        AssetDir.cpp \
        AssetManager.cpp \
        AssetManager2.cpp \
        AttributeResolution.cpp \
        ChunkIterator.cpp \
        LoadedArsc.cpp \
        LocaleData.cpp \
        misc.cpp \
        ObbFile.cpp \
        ResourceTypes.cpp \
        ResourceUtils.cpp \
        StreamingZipInflater.cpp \
        TypeWrappers.cpp \
        Util.cpp \
        ZipFileRO.cpp \
        ZipUtils.cpp

SOURCES := $(foreach source, $(SOURCES), libs/androidfw/$(source))
CXXFLAGS += -DSTATIC_ANDROIDFW_FOR_TOOLS -std=gnu++11
CPPFLAGS += -Ilibs/androidfw/include
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0 \
           -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -lz \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -lziparchive -lutils -lcutils -llog -lbase

build: $(SOURCES)
	mkdir --parents debian/out
	$(CXX) $^ -o debian/out/$(NAME).so.0 $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
	ln -s $(NAME).so.0 debian/out/$(NAME).so