NAME = aapt2
SOURCES = \
	cmd/Compile.cpp \
	cmd/Diff.cpp \
	cmd/Dump.cpp \
	cmd/Link.cpp \
	cmd/Optimize.cpp \
	cmd/Util.cpp \
	compile/IdAssigner.cpp \
	compile/InlineXmlFormatParser.cpp \
	compile/NinePatch.cpp \
	compile/Png.cpp \
	compile/PngChunkFilter.cpp \
	compile/PngCrunch.cpp \
	compile/PseudolocaleGenerator.cpp \
	compile/Pseudolocalizer.cpp \
	compile/XmlIdCollector.cpp \
	configuration/ConfigurationParser.cpp \
	filter/AbiFilter.cpp \
	filter/ConfigFilter.cpp \
	flatten/Archive.cpp \
	flatten/TableFlattener.cpp \
	flatten/XmlFlattener.cpp \
	io/BigBufferStreams.cpp \
	io/File.cpp \
	io/FileInputStream.cpp \
	io/FileSystem.cpp \
	io/StringInputStream.cpp \
	io/Util.cpp \
	io/ZipArchive.cpp \
	link/AutoVersioner.cpp \
	link/ManifestFixer.cpp \
	link/ProductFilter.cpp \
	link/PrivateAttributeMover.cpp \
	link/ReferenceLinker.cpp \
	link/TableMerger.cpp \
	link/XmlCompatVersioner.cpp \
	link/XmlNamespaceRemover.cpp \
	link/XmlReferenceLinker.cpp \
	optimize/ResourceDeduper.cpp \
	optimize/VersionCollapser.cpp \
	process/SymbolTable.cpp \
	proto/ProtoHelpers.cpp \
	proto/TableProtoDeserializer.cpp \
	proto/TableProtoSerializer.cpp \
	split/TableSplitter.cpp \
	text/Unicode.cpp \
	text/Utf8Iterator.cpp \
	unflatten/BinaryResourceParser.cpp \
	unflatten/ResChunkPullParser.cpp \
	util/BigBuffer.cpp \
	util/Files.cpp \
	util/Util.cpp \
	ConfigDescription.cpp \
	Debug.cpp \
	DominatorTree.cpp \
	Flags.cpp \
	java/AnnotationProcessor.cpp \
	java/ClassDefinition.cpp \
	java/JavaClassGenerator.cpp \
	java/ManifestClassGenerator.cpp \
	java/ProguardRules.cpp \
	LoadedApk.cpp \
	Locale.cpp \
	Resource.cpp \
	ResourceParser.cpp \
	ResourceTable.cpp \
	ResourceUtils.cpp \
	ResourceValues.cpp \
	SdkConstants.cpp \
	StringPool.cpp \
	xml/XmlActionExecutor.cpp \
	xml/XmlDom.cpp \
	xml/XmlPullParser.cpp \
	xml/XmlUtil.cpp \
	Main.cpp \

SOURCES := $(foreach source, $(SOURCES), tools/aapt2/$(source))
SOURCES += debian/out/proto/frameworks/base/tools/aapt2/Resources.pb.cc \
	debian/out/proto/frameworks/base/tools/aapt2/ResourcesInternal.pb.cc
CXXFLAGS += -fno-exceptions
CPPFLAGS += -Ilibs/androidfw/include -Itools/aapt2 \
	-Idebian/out/proto/frameworks/base/tools/aapt2 \
	-DAAPT_VERSION=\$(ANDROID_BUILD_TOOLS_VERSION)\ \
	-DSTATIC_ANDROIDFW_FOR_TOOLS
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-lexpat -lpng -lprotobuf-lite \
	-Wl,-rpath-link=debian/out -Ldebian/out -landroidfw \
	-L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-lutils -lbase -lziparchive

build: $(SOURCES)
	$(CXX) $^ -o debian/out/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)

debian/out/proto/frameworks/base/tools/aapt2/Resources.pb.cc: tools/aapt2/Resources.proto
	mkdir --parents debian/out/proto/frameworks/base/tools/aapt2
	protoc --cpp_out=debian/out/proto/frameworks/base/tools/aapt2 \
		--proto_path=tools/aapt2 $<

debian/out/proto/frameworks/base/tools/aapt2/ResourcesInternal.pb.cc: tools/aapt2/ResourcesInternal.proto
	mkdir --parents debian/out/proto/frameworks/base/tools/aapt2
	protoc --cpp_out=debian/out/proto/frameworks/base/tools/aapt2 \
		--proto_path=tools/aapt2 $<
